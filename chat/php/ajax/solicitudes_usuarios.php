<?php
	if(!isset($_SESSION['id_usuario'])){
		session_start();
		include("../conexion.php");
	}else{
		include("php/conexion.php");		
	}
	$id_usuario = $_SESSION['id_usuario'];
	$consulta = "SELECT 
					s_u.id_solicitud, 
					CONCAT(u.nombre_usuario , ' (' , u.alias , ')' ),
					u.foto
				FROM usuarios u
				RIGHT JOIN solicitudes_usuarios s_u ON u.id_usuario = s_u.id_usuario_envia 
				WHERE s_u.id_usuario_destino = $id_usuario AND s_u.cancelada = 0 AND s_u.confirmada = 0";
	$resultados = $conexion -> query($consulta) or die("Error al listar las solicitudes : " . $conexion -> error);
	
	while ( $r = $resultados -> fetch_row()) {
		echo '<div class="usuario_tarjeta">';
			echo '<span>' . $r[1] . '</span>';
			echo '<img src="' . $r[2] . '" class="imagen_tarjeta">';
			echo '<button type="button" class="add_usuario" onclick="responde_solicitud(' . $r[0] . ',1);"><b>+</b></button>';
			echo '<button type="button" class="add_usuario" onclick="responde_solicitud(' . $r[0] . ',0);"><b>x</b></button>';
		echo '</div>';
	}
?>