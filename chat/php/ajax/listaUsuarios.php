<?php
	if(!isset($_SESSION['id_usuario'])){
		session_start();
	}
	
	if(isset($_GET['busqueda'])){
		include("../conexion.php");
	}else{
		include("php/conexion.php");
	}

	$id_usuario = $_SESSION['id_usuario'];

	$consulta = "SELECT 
					u.id_usuario, 
					CONCAT( u.nombre_usuario, ' (', u.alias, ')' ),
					u.foto
				FROM usuarios u
				WHERE u.id_usuario != $id_usuario
				AND u.id_usuario NOT IN(SELECT 
											IF(s_u.id_usuario_destino = $id_usuario, 
												s_u.id_usuario_envia, 
												IF(s_u.id_usuario_envia = $id_usuario,
													s_u.id_usuario_destino,
													0
												)
											)
										FROM solicitudes_usuarios s_u 
										WHERE s_u.id_usuario_envia = $id_usuario
										OR s_u.id_usuario_destino = $id_usuario)";

	if(isset($_GET['busqueda'])){
		$caracteres = $_GET['busqueda'];
		$consulta .= " AND (u.nombre_usuario LIKE '%" . $caracteres . "%' OR u.alias LIKE '%" . $caracteres . "%')";
	}
	$resultados = $conexion -> query($consulta) or die("Error al consultar usuarios : <br> " . $conexion -> error);

	while ($r = $resultados -> fetch_row()) {
		echo '<div class="usuario_tarjeta">';
			echo '<span>' . $r[1] . '</span>';
			echo '<img src="' . $r[2] . '" class="imagen_tarjeta">';
			echo '<button type="button" class="add_usuario" onclick="agregar_usuario(' . $r[0] . ');"><b>+</b></button>';
		echo '</div>';
	}
?>