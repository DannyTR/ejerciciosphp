<?php
?>

<!DOCTYPE html>
<html>
<head>
	<title>Crud Productos</title>


	<!--Incluimos librería JQuery-->
	<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
	<!--Incluimos nuestras funciones JS-->
	<script type="text/javascript" src="js/mainFunciones.js"></script>


	<link href="css/style.css" rel="stylesheet" type="text/css">

</head>
<body>
	<h1>Productos</h1>
<!--Aqui se enlistan los productos-->	
	<div id="contenido_lista">
		<?php include("php/listadoProductos.php");?>
	</div>

<br>
<!--Botón para agregar un nuevo producto-->
	<button type="button" onclick="abre_nuevo_form();">Agregar</button>

</body>
</html>

<!--<style type="text/css">
	/*th{padding: 10px; background: rgba(225,0,0,.5); color: white;}*/
	th 
{     
	font-size: 13px;     
	font-weight: normal;     
	padding: 8px;     
	background: #b9c9fe;
    border-top: 4px solid #aabcfe;    
    border-bottom: 1px solid #fff; 
    color: #039; 
}

td {    
	padding: 8px;     
	background: #e8edff;     
	border-bottom: 1px solid #fff;
    color: #669;    
    border-top: 1px solid transparent; 
}	
</style>-->

