<?php

//incluir archivo de conexión
	include("conexion.php");

//crear consulta para listar datos
	$consulta = "SELECT
						id_prod,
						nombre,
						marca,
						precio,
						observaciones
						FROM prods";

	$ejecuta = $conexion -> query($consulta) or die("Error al listar productos <br> :" . $conexion -> error);				

?>

<table id="lista_productos">
	<tr>
		<th>ID</th>
		<th>NOMBRE</th>
		<th>MARCA</th>
		<th>PRECIO</th>
		<th>OBSERVACIONES</th>	
		<th colspan="2">ACCIONES</th>
	</tr>

	<?php

		while( $arreglo_resultados = $ejecuta -> fetch_row() ){
		echo '<tr>';
		echo '<td>' . $arreglo_resultados[0] . '</td>';
		echo '<td>' . $arreglo_resultados[1] . '</td>';
		echo '<td>' . $arreglo_resultados[2] . '</td>';
		echo '<td>' . $arreglo_resultados[3] . '</td>';
		echo '<td>' . $arreglo_resultados[4] . '</td>';
		//botón para editar
		echo '<td> <button type="button" onclick="formProducto('.$arreglo_resultados[0].');">';
		echo 'Editar </button></td>';
		echo '<td> <button type="button" onclick="eliminarProducto('.$arreglo_resultados[0].');">';
		echo 'Eliminar </button></td>';

		echo '</tr>';
	}

	?>

		
</table>