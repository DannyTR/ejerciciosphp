<?php

$articles = [ "First post", "Another post", "Read this!" ];

var_dump($articles);

var_dump($articles[0]);
var_dump($articles[2]);
var_dump($articles[1]);

//-----------------------------

echo "<br>";

$articles = [
    "first"  => "First post",
    "second" => "Another post",
    "third"  => "Read this!"
];

var_dump($articles);

var_dump($articles["second"]);
