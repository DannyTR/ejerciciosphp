<?php
//CLAUSULAS CONDICIONALES
echo 'CLAUSULAS CONDICIONALES';

echo '<br><br>CLASULA IF<br>';

$edad = 16; //definimos edad con valor de 16

if ($edad < 18 ) { //si la edad es menor a 18, imprimira el mensaje "no eresmayor de edad"
  echo "No eres mayor de edad";
}
//-----------------------------------------------------------------------------------
//CLAUSULA IF ELSE
echo '<br><br>CLASULA IF ... ELSE<br>';

$edad2 = 20;

if ($edad2 < 18 ) { 
  echo "No eres mayor de edad";
}
else {
	echo "Eres mayor de edad";
}
//-----------------------------------------------------------------------------------
//CLAUSULA IF ELSEIF ELSE
echo '<br><br>CLASULA IF ... ELSEIF ... ELSE<br>';

$edad3 = 20;

if ($edad3 < 18 ) { 
  echo "No eres mayor de edad";
}
elseif($edad3 == 20){
	echo "tienes 20 años";
}
else {
	echo "Eres mayor de edad";
}

//-----------------------------------------------------------------------------------
echo '<br><br>CLASULA SWITCH<br>';

$favcolor = "red";

switch ($favcolor) {
  case "red":
    echo "Your favorite color is red!";
    break;
  case "blue":
    echo "Your favorite color is blue!";
    break;
  case "green":
    echo "Your favorite color is green!";
    break;
  default:
    echo "Your favorite color is neither red, blue, nor green!";
}

//-----------------------------------------------------------------------------------
//LOOPS
//WHILE LOOP
echo '<br><br>LOOP WHILE<br>';

$x = 1; //declaramos la variable x en 1
 
while($x <= 5) { //mientras x (igual a 1) sea menor o igual a 5 se ejecutara el bloque de codigo
  echo "The number is: $x <br>"; //imprime la cadena the number es: y la variable
  $x++;
} 

echo '<br>Segundo while , ahora x vale 6<br>';

while($x <= 100) { //mientras x (que vale 6) sea menor o igual a 100
  //imprime el numero
  echo "The number is: $x <br>";
  //incrementa la variable x en 10
  $x+=10;
}

//----------------------------------------------------------------------------------
//DO WHILE LOOP
echo '<br><br>LOOP DO WHILE<br>';

$y = 1;

do {
  echo "The number is: $y <br>";
  $y++;
} while ($y <= 5);

//----------------------------------------------------------------------------------
//FOR LOOP
echo '<br><br>LOOP FOR<br>';

for ($contador = 0; $contador <= 10; $contador++) {
	echo "El contador vale: $contador <br>";
}

//---------------------------------------------------------------------------------
//FOREACH LOOP
echo '<br><br>LOOP FOR EACH<br>';

$colores = array("rojo", "verde", "azul", "amarillo"); 

foreach ($colores as $color) {
  echo "$color <br>";
}

//----------------------------------------------------------------------------------
//BREAK
echo '<br><br>Clausula BREAK<br>';

for($cont1=0; $cont1 < 10; $cont1++){
	echo "el numero es: $cont1 <br>";
}
echo '<br>ya fuera del primer loop sin la clausula break<br>';

echo '<br>----------------<br><br>';

for ($cont = 0; $cont < 10; $cont++) { //DEBERIA IMPRIMIR HASTA EL NUMEORO )
  if ($cont == 4) {
    break; //AQUI SE ROMPE EL CICLO CUANDO LA COMDICION IF SEA TRUE
  }
  echo "El número es: $cont <br>"; // SOLO IMPRIME HASTA EL NUMERO 3
}
echo 'Ya fuera del loop con la clausula BREAK';

//----------------------------------------------------------------------------------
//CONTINUE
echo '<br><br>Clausula CONTINUE<br>';

for($cont2=0; $cont2 < 10; $cont2++){
	echo "el numero es: $cont2 <br>";
}
echo '<br>ya fuera del primer loop sin la clausula CONTINUE<br>';

echo '<br>----------------<br><br>';

for ($cont2 = 0; $cont2 < 10; $cont2++) { //DEBERIA IMPRIMIR HASTA EL NUMEORO )
  if ($cont2 == 4) {
  	echo "<b>Aqui se omite el valor $cont2</b><br>";
    continue; //AQUI SE SALTA LA CONDICIOMN TRUE PERO CONTINUA CON EL RESTO
  }
  echo "El número es: $cont2 <br>"; // IMPRIME DEL 5 en ADELANTE
}
echo 'Ya fuera del loop con la clausula CONTINUE';

//----------------------------------------------------------------------------------
//WHILE CON BREAK Y CONTINUE

echo '<br><br>WHILE CON BREAK Y CONTINUE<br>';

$cont3 = 0;
 
while($cont3 <= 10) { //deberia imprimir hasta el 10
	if($cont3 == 4 || $cont3 == 6) { //si cont3 vale 4 o 6, saltara esas vueltas
		$cont3++;
    	continue;
    }
    elseif($cont3 == 9){ //si cont3 vale 9 sale del loop y ya no imprime el 10
		break;
    }
    echo "Contador vale: $cont3 <br>"; //deberia imprimir, 1,2,3,5,7,8
    $cont3++;
} 

?>